#!/usr/bin/env python3

import sys

current_key = None
is_good = True
word_sum = 0

for line in sys.stdin:
    try:
        key, tag = line.strip().split('\t', 1)
        tag = int(tag)
    except ValueError as e:
        continue

    if current_key != key:
        if current_key and is_good:
            print(current_key, word_sum, sep='\t')
        word_sum = 0
        current_key = key
        is_good = True
    if tag == 1:
        is_good = False
    else:
        word_sum += 1

if current_key and is_good:
    print(current_key, word_sum, sep='\t')
