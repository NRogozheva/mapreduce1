#!/usr/bin/env python3

import sys
import re

sys.setdefaultencoding('utf-8')

for line in sys.stdin:
    try:
        article_id, text = line.strip().split('\t', 1)
    except ValueError as e:
        continue

    words = text.split(" ")

    for word in words:
        if len(word) >= 6:
            res_word = ""
            for letter in word:
                if letter.isalpha():
                    res_word += letter
            if 6 <= len(res_word) <= 9:
                print(res_word.lower(), int(res_word[0].islower()), sep='\t')
